# BrightTALK's Style Guide

## Run the demo (Docker)
```
docker build -t brighttalk/style-guide .
docker run --rm -v ${}-p 80:80 brighttalk/style-guide

# navigate to http://localhost/demo
```

Open [http://127.0.0.1/demo/](http://127.0.0.1/demo/) in your browser

## Usage
```bower install git@bitbucket.org:brighttalktech/style-guide.git```

## Contribute

> Pro-tip: Never work directly on master. Use any branching model that suits you best. Suggestion: [Git-Flow](http://nvie.com/posts/a-successful-git-branching-model/)

### Installation

#### Clone the repository
```
git clone git@bitbucket.org:brighttalktech/style-guide.git
```

#### Install its dependencies
```
npm install && bower install
```

### Build

#### Automatic build during dev (watch)
```
grunt watch
```

#### On demand build
```
grunt build
```
