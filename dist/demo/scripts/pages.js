/*jslint maxerr: 50, indent: 4*/
/*globals $, window*/

$(function () {

    'use strict';

    $('.exampleModal').show();

    $('#modalDemoButton').on('click', function () {

        $('.liveDemoModal').show();
    });

    $('.liveDemoModal').on('click', function () {

        $('.liveDemoModal').hide();
    });

    $('.do-not-disable').on('click', function () {

        $('button').not(this).attr('disabled', function (i, current) {

            return current === 'disabled' ? false : true;
        });
    });
});
