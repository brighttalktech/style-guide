/*jslint maxerr: 50, indent: 4*/
/*globals $*/

$(function () {

    'use strict';

    function getParentLabel(element) {
        return $(element).parents('.chkbox:eq(0)');
    }

    $('.chkbox input:checkbox').on('click', function () {

        getParentLabel(this).toggleClass('chkbox-checked');
    })
        .each(function () {
            if ($(this).prop('checked')) {
                getParentLabel(this).addClass('chkbox-checked');
            }
        });
});
