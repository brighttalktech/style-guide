/*jslint maxerr: 50, indent: 4*/
/*globals $*/

$(function () {

    'use strict';

    function getParentLabel(element) {
        return $(element).parents('.radio:eq(0)');
    }

    $('.radio input:radio').on('click', function () {

        getParentLabel(this).toggleClass('radio-checked');

        $('.radio input:radio').each(function () {
            if ($(this).prop('checked')) {
                getParentLabel(this).addClass('radio-checked');
            } else {
                getParentLabel(this).removeClass('radio-checked');
            }

        });
    })
        .each(function () {
            if ($(this).prop('checked')) {
                getParentLabel(this).addClass('radio-checked');
            }
        });
});
