/*globals $*/

$(function () {

    'use strict';

    var togglingMenu = false;

    $('.jsProfileMenuToggleButton').on('click', function () {

        $(this).toggleClass('profile-menu-close-button');
        $(this).siblings('.profile-menu').toggle();
        togglingMenu = true;
    });

    $('body').on('click', function () {

        if (togglingMenu === false) {

            $('.profile-menu-toggle-button').removeClass('profile-menu-close-button');
            $('.profile-menu').hide();
        }

        togglingMenu = false;
    });
});