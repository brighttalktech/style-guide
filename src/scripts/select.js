/*jslint maxerr: 50, indent: 4*/
/*globals $*/

$(function () {

    'use strict';

    $('.select').on('change', function () {

        if($(this).val() == '') {
          $(this).toggleClass('selected', false);
        } else {
          $(this).toggleClass('selected', true);
        }
    });
});
