/*globals $*/

$(function () {

    'use strict';

    var openingMenu = null;

    $('body').on('click', function () {

        if (openingMenu === null) {

            $('.breadcrumb-menu').addClass('breadcrumb-menu-hidden');
        } else {

            $('.breadcrumb-menu').not(openingMenu).addClass('breadcrumb-menu-hidden');
            openingMenu = null;
        }
    })
        .on('click', '.breadcrumb-button', function () {

            openingMenu = $(this).siblings('.breadcrumb-menu')
            openingMenu.toggleClass('breadcrumb-menu-hidden');

        });
});
