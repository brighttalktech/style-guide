/*jslint maxerr: 50, indent: 4*/
/*globals $*/

$(function () {

    'use strict';

    $('.profile-card-image-options-button, .profile-card-image-options').on('click', function () {

        var parentCard = $(this).parents('.profile-card:eq(0)');
        parentCard.find('.profile-card-image-options-button').toggleClass('profile-card-image-options-button-hidden');
        parentCard.find('.profile-card-image-options').toggleClass('profile-card-image-options-hidden');
    });

});
