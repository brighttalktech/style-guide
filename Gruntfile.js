module.exports = function(grunt) {

    "use strict";
    grunt.initConfig({
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: false,
                undef: true,
                eqnull: true,
                browser: true,
                boss: true,
                node: true,
                es5: true,
                onevar: true,
                globals: {
                    define: true,
                    Stomp: true,
                    SockJS: true,
                    _: true,
                    Backbone: true
                }
            },
            files: ["GruntFile.js", "src/scripts/**/*.js"]
        },
        watch: {
            build: {
                files: ["src/**/*.*"],
                tasks: ["build"],
                options: {
                    spawn: true
                }
            }
        },
        less: {
            development: {
                options: {
                    paths: ["src/less", "src/css"]
                },
                files: {
                    "dist/css/main.css": "src/less/main.less"
                }
            }
        },
        includes: {
            files: {
                src: ['**/*.html'],
                dest: 'dist/demo',
                cwd: 'src/demo/pages',
                options: {
                    includePath: "src/demo/includes",
                    includeRegexp: /^(\s*)include\s+'(\S+)'\s*$/,
                    silent: true
                }
            }
        },
        copy: {
            demoCss: {
                expand: true,
                src: '**/*.*',
                cwd: 'src/demo/pages/css',
                dest: 'dist/demo/css'
            },
            styleGuideScripts: {
                expand: true,
                src: '**/*.*',
                cwd: 'src/scripts',
                dest: 'dist/scripts'
            },
            bootstrapScripts: {
                expand: true,
                src: ['alert.js', 'dropdown.js'],
                cwd: 'bower_components/bootstrap/js',
                dest: 'dist/scripts'
            },
            fontAwesome: {
                expand: true,
                src: '**/*.*',
                cwd: 'bower_components/components-font-awesome/fonts',
                dest: 'dist/fonts'
            },
            fonts: {
                expand: true,
                src: '**/*.*',
                cwd: 'src/fonts',
                dest: 'dist/fonts'
            },
            demoScripts: {
                expand: true,
                src: '**/*.*',
                cwd: 'src/demo/pages/scripts',
                dest: 'dist/demo/scripts'
            },
            images: {
                expand: true,
                src: '**/*.*',
                cwd: 'src/images',
                dest: 'dist/images'
            },
            demoImages: {
                expand: true,
                src: '**/*.*',
                cwd: 'src/demo/pages/images',
                dest: 'dist/demo/images'
            },
        },
        clean: {
            dist: {
                src: ["dist/**/*"]
            }
        }

    });

    grunt.registerTask('default', ['build']);
    grunt.registerTask('build', ['clean:dist', 'copy:build', 'less', 'includes']);
    grunt.registerTask('copy:build', ['copy:demoCss', 'copy:fonts', 'copy:fontAwesome', 'copy:styleGuideScripts', 'copy:bootstrapScripts', 'copy:demoScripts', 'copy:images', 'copy:demoImages'])
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
};
